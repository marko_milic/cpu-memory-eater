package core.constants;

public class Constants {
    public static final int BYTE_FACTOR = 1024;
    public static final int MEGABYTE_FACTOR = 1024 * 1024;
    public static final int MEMORY_REQUIRED_BY_JMV = 64 * MEGABYTE_FACTOR;
}
