package core;

import core.constants.Colors;
import core.constants.Constants;

public class Validator {

    public static boolean validate(LoaderParams params) {
        if (Runtime.getRuntime().maxMemory() < params.getMemoryToEat().getBytes()) {
            System.out.println(Colors.Font.ANSI_RED + "============ ERROR =============");
            System.out.println("JMV Max Memory: " + Runtime.getRuntime().maxMemory() / Constants.MEGABYTE_FACTOR + "mb");
            System.out.println("JMV Required Memory: " + Constants.MEMORY_REQUIRED_BY_JMV / Constants.MEGABYTE_FACTOR + "mb");
            System.out.println("Memory to eat: " + params.getMemoryToEat());
            System.out.println("Maximum size of heap memory is less than desired memory to eat.");
            System.out.println("----> SUGGESTION <----");
            System.out.println("Please adjust memory in JVM with parameter \"-Xmx1000m\", where 1000 is amount of memory available to JVM");
            System.out.println("============ ERROR =============");
            return false;
        }

        return true;
    }
}
