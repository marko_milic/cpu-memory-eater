package core;

import capacity.Capacity;

public class LoaderParams {
    private int cores ;
    private double load;
    private Capacity memoryToEat;

    public LoaderParams(int cores, double load, Capacity memoryToEat) {
        this.cores = cores;
        this.load = load;
        this.memoryToEat = memoryToEat;
    }

    public int getCores() {
        return cores;
    }

    public double getLoad() {
        return load;
    }

    public Capacity getMemoryToEat() {
        return memoryToEat;
    }
}
