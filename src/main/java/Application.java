import capacity.Capacity;
import core.LoadManager;
import core.LoaderParams;
import core.Validator;
import core.constants.Colors;
import core.cpu.CPULoader;
import core.memory.MemoryLoader;


public class Application {

    public static void main(String[] args) {
        int cores = 6;
        double load = 1;
        Capacity reservedMemory = Capacity.ofMegabytes(512);
        Capacity memoryToOccupy = Capacity.ofBytes(Runtime.getRuntime().maxMemory() - reservedMemory.getBytes());

        LoaderParams params = new LoaderParams(cores, load, memoryToOccupy);

        System.out.println(Colors.Font.ANSI_BLUE + "--------- Loader Properties ---------");
        System.out.println("JMV Memory: " + Capacity.ofBytes(Runtime.getRuntime().maxMemory()));
        System.out.println("Reserved Memory: " + reservedMemory);
        System.out.println("Memory to occupy: " + params.getMemoryToEat());
        System.out.println("Available Processors: " + Runtime.getRuntime().availableProcessors());
        System.out.println("Load cores: " + cores);
        System.out.println("Load Factor per Thread: " + params.getLoad());
        System.out.println("--------- Loader Properties ---------");

        if (!Validator.validate(params))
            System.exit(1);

        CPULoader cpuLoader = CPULoader.create()
                .withCores(params.getCores())
                .withLoad(params.getLoad());

        MemoryLoader memoryLoader = MemoryLoader.create()
                .withBytesToLoad(params.getMemoryToEat().getBytes());

        LoadManager.getInstance()
                .register(cpuLoader)
                .register(memoryLoader)
                .startLoaders();

    }
}
