package capacity;

public class Capacity {
    public static final Capacity MEGABYTE = Capacity.ofMegabytes(1);
    public static final Capacity KILOBYTE = Capacity.ofKilobytes(1);
    public static final Capacity GIGABYTE = Capacity.ofGigabytes(1);

    private long value;

    public Capacity(long value) {
        this.value = value;
    }

    public long getBytes() {
        return value;
    }

    public static Capacity ofGigabytes(double gigabyte) {
        if (gigabyte < 0)
            throw new IllegalArgumentException();
        return new Capacity(Converter.fromGigabyte(gigabyte));
    }

    public static Capacity ofMegabytes(double gigabyte) {
        if (gigabyte < 0)
            throw new IllegalArgumentException();

        return new Capacity(Converter.fromMegabyte(gigabyte));
    }

    public static Capacity ofKilobytes(double gigabyte) {
        if (gigabyte < 0)
            throw new IllegalArgumentException();

        return new Capacity(Converter.fromKilobyte(gigabyte));
    }

    public static Capacity ofBytes(long bytes) {
        if (bytes < 0)
            throw new IllegalArgumentException();

        return new Capacity(bytes);
    }

    @Override
    public String toString() {
        return Converter.toGigabyte(value) + "GB";
    }
}
