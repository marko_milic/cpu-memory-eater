package core.memory;

import capacity.Capacity;
import core.Loadable;

import java.util.Vector;

public class MemoryLoad implements Loadable {
    private Vector vector;
    private long bytesToLoad;

    public MemoryLoad(long bytesToEat) {
        this.bytesToLoad = bytesToEat;
        vector = new Vector();
    }

    @Override
    public void load() {
        System.out.println("Hello from Memory Loader!" + " THREAD: " + Thread.currentThread().getName());
        while (true) {
            if (bytesToLoad > 0) {
                vector.add(allocate((int) Capacity.MEGABYTE.getBytes()));
                bytesToLoad -= Capacity.MEGABYTE.getBytes();
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private byte[] allocate(int bytes) {
        return new byte[bytes];
    }
}
