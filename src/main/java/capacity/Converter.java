package capacity;

public class Converter {
    private static long BASE = 1024;

    public static long fromGigabyte(double gigabyte) {
        return Math.round(gigabyte * Math.pow(BASE, 3));
    }

    public static long fromMegabyte(double megabyte) {
        return Math.round(megabyte * Math.pow(BASE, 2));
    }

    public static long fromKilobyte(double kilobyte) {
        return Math.round(kilobyte * Math.pow(BASE, 1));
    }

    public static double toGigabyte(double bytes) {
        return Math.round((bytes / Math.pow(BASE, 3)) * 100.0) / 100.0;
    }

}
